import { createStackNavigator } from "react-navigation";
import MainScreen from "../screens/AppScreens/MainScreen";
import Profile from "../screens/SharedScreens/Footer/Account/Profile";
import ForgotPassword from "../screens/AuthScreens/ForgotPassword";
import ChangePassword from "../screens/AuthScreens/ChangePassword";

export const AppStack = createStackNavigator(
  {
    Main: {
      screen: MainScreen,
    },
    Account: {
      screen: Profile,
    },
    Forgot: {
      screen: ForgotPassword,
    },
    Change: {
      screen: ChangePassword,
    },
  })

