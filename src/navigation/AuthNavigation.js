
import { createStackNavigator } from "react-navigation";
import LoginScreen from "./../screens/AuthScreens/LoginScreen";
import Verification from "../screens/AuthScreens/Verification";
import SignUpScreen from "../screens/AuthScreens/SignUpScreen";

export const AuthStack= createStackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    Signup:{
      screen: SignUpScreen
    },
    OTP: {
      screen: Verification,
      params:{
        mobileNum: ''
      }
    },
  })