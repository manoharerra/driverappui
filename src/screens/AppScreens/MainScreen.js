import React, { Component } from 'react';
import { View, Image } from 'react-native';
import styles from "../../styles/LoginStyles";
import { Form, Picker, Container, Button, Item, Input, Text, Content } from 'native-base';
import FooterPart from '../SharedScreens/Footer/FooterPart';
import { Constants, MapView, Location, Permissions } from 'expo';

export default class MainScreen extends Component {

	static navigationOptions = {
		header: null
	}
	
	state = {
		mapRegion: null,
		hasLocationPermissions: false,
		locationResult: null
	};
	constructor(props) {
		super(props)
		this.state = {
			modalShow: this.props.modalShow
		}
	}
	componentDidMount() {
		this._getLocationAsync();
	}

	_handleMapRegionChange = mapRegion => {
		console.log(mapRegion);
		this.setState({ mapRegion });
	};

	_getLocationAsync = async () => {
		let { status } = await Permissions.askAsync(Permissions.LOCATION);
		if (status !== 'granted') {
			this.setState({
				locationResult: 'Permission to access location was denied',
			});
		} else {
			this.setState({ hasLocationPermissions: true });
		}

		let location = await Location.getCurrentPositionAsync({});
		this.setState({ locationResult: JSON.stringify(location) });

		// Center the map on the location we just fetched.
		this.setState({ mapRegion: { latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 } });
	};

	componentWillReceiveProps(props) {
		this.setState({
			modalShow: this.props.modalShow
		})
		console.log(props);
	}
	onValueChange(value: string) {
		this.setState({
			selected: value
		});
	}
	render() {
		return (
			<Container>
				<Content>
					{
						this.state.locationResult === null ?
							<Text>Finding your current location...</Text> :
							this.state.hasLocationPermissions === false ?
								<Text>Location permissions are not granted.</Text> :
								this.state.mapRegion === null ?
									<Text>Map region doesn't exist.</Text> :
									<MapView
										style={{ alignSelf: 'stretch', height: 800 }}
										region={this.state.mapRegion}
										onRegionChange={this._handleMapRegionChange}
									/>
					}
				</Content>
				<FooterPart footerProps={this.props} />
			</Container >
		)
	}

}