import React, { Component } from 'react';
import { View, TouchableHighlight, Modal, Alert, Image, } from 'react-native';
import { Form, Label, Picker, Container, Header, Title, Content, Footer, FooterTab, Button, Left, Item, Input, Right, Body, Icon, Text, H1, H2, H3, Card, CardItem } from 'native-base';


export default class LoginScreen extends Component {
	static navigationOptions = {
		header: null
	}
	constructor(props) {
		super(props)
		this.state = {
			modalShow: this.props.modalShow
		}
	}
	componentWillReceiveProps(props) {
		this.setState({
			modalShow: this.props.modalShow
		})
		console.log(props);
	}
	onValueChange(value: string) {
		this.setState({
			selected: value
		});
	}
	render() {
		return (
			<Container>
				<Content>
					<Image
						style={{ alignSelf: 'center', marginTop: '50%' }}
						source={require('../../assets/images/logo.png')}
					/>
					<Text style={{ alignSelf: 'center', color: '#737373', fontSize: 20, fontWeight: 'bold' }}>DRIVER APP</Text>

					<Form style={{ marginTop: '20%' }}>
						<Item style={{ marginTop: '2%', marginLeft: '10%', marginRight: '10%',borderColor:'#363636' }}>
							<Label>USER NAME</Label>
							<Input />
						</Item>
						<Item style={{ marginTop: '2%', marginLeft: '10%', marginRight: '10%',borderColor:'#363636' }}>
							<Label>PASSWORD</Label>
							<Input secureTextEntry={true}
							/>
						</Item>
					</Form>
					<Card transparent style={{ marginTop: '2%' }}>
						<CardItem>
							<Left>
								<Button transparent onPress={() => { this.props.navigation.navigate('Forgot') }}>
									<Text style={{ fontWeight: 'bold', color: 'red' }}>
										Forgot Password ?
								</Text>
								</Button>
							</Left>
							<Right>
								<Button transparent onPress={() => { this.props.navigation.navigate('Signup') }}>
									<Text style={{ fontWeight: 'bold', color: 'green' }}>
										Create Account
								</Text>
								</Button>
							</Right>
						</CardItem>
					</Card>
					<Button full
						onPress={() => { this.props.navigation.navigate('Main') }}
						style={{ backgroundColor: "orange", marginTop: '2%', marginLeft: '10%', marginRight: '10%', }}
					>
						<Text style={{ color: 'white', fontWeight: '900' }}>LOGIN</Text>
					</Button>
				</Content>
			</Container>
		)
	}

}