import React, { Component } from 'react';
import { View, TouchableHighlight, Modal, Alert, Image } from 'react-native';
import styles from "../../styles/LoginStyles";
import { Form, Picker, Container, Header, Title, Content, Footer, FooterTab, Button, Left, Item, Input, Right, Body, Icon, Text, H1, H2, H3 } from 'native-base';


export default class SignUpScreen extends Component {
	constructor(props) {
		super(props)
		this.state = {
			modalShow: this.props.modalShow
		}
	}
	onValueChange(value: string) {
		this.setState({
			selected: value
		});
	}
	render() {
		return (
			<Container>
				<View style={styles.logoContainer}>
					<Image
						source={require('../../assets/images/logo.png')}
					/>
					<Form style={{ marginLeft: '10%', marginRight: '10%' }}>
						<Item regular
							style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%' }}>
							<Input placeholder='First Name' />
						</Item>
						<Item regular style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%' }}>
							<Input placeholder='Last Name' />
						</Item>
						<Item regular style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%' }}>
							<Input placeholder='Email Id' />
						</Item>
						<Item regular
							secureTextEntry={!this.state.showPassword}
							style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%' }}>
							<Input placeholder='Enter Password' secureTextEntry={true} />
						</Item>
						<Item regular style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%' }}>
							<Input placeholder='Confirm Password' secureTextEntry={true} />
						</Item>
						<Item regular style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%' }}>
							<Input placeholder='Phone Number' />
						</Item>
						<Item regular style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%' }}>
							<Picker
								placeholder="Gender"
								mode="dropdown"
								selectedValue={this.state.selected}
								onValueChange={this.onValueChange.bind(this)}
							>
								<Picker.Item label="Male" value="key0" />
								<Picker.Item label="Female" value="key1" />
								<Picker.Item label="Others" value="key2" />
							</Picker>
						</Item>
						<Item regular style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%' }}>
							<Input placeholder='Date Of Birth' />
						</Item>
					</Form>
					<Button full
						onPress={() => { this.props.navigation.navigate('CustomerDetails') }}
						style={{ backgroundColor: "#363636", marginTop: 20, marginLeft: 20, marginRight: 20, marginBottom: 50 }}
					>
						<Text style={{ color: 'white', fontWeight: '900' }}>Sign Up</Text>
					</Button>
				</View>
			</Container>
		)
	}

}