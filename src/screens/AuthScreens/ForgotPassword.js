import React, { Component } from 'react';
import { View, TouchableHighlight, Modal, Alert, Image } from 'react-native';
import { Form, Picker, Container, Header, Title, Content, Footer, FooterTab, Button, Left, Item, Input, Right, Body, Icon, Text, H1, H2, H3 } from 'native-base';

export default class ForgotPassword extends Component {
	static navigationOptions = {
		header: null
	}
	constructor(props) {
		super(props)
		this.state = {
			password: '',
			showPassword: false,

		}
	}
	render() {
		return (
			<Container>
				<Content>
					<View style={{ marginTop: '30%' }}>
						<View style={{}}>
							<View style={{}}>
								<View style={{}}>
									<Image style={{ alignSelf: 'center' }}
										source={require('../../assets/images/logo.png')}
									/>
									<Text style={{ alignSelf: 'center', color: '#737373', fontSize: 20, fontWeight: 'bold' }}>DRIVER APP</Text>

								</View>
								<Text
									style={{ marginTop: '20%', marginLeft: 20, marginRight: 10, }}
								><H1 style={{ color: '#363636', fontWeight: '500' }}>Enter Your Registered Email or Phone Number for OTP</H1>
								</Text>
								<Form style={{ marginLeft: 20, marginRight: 20, }}>
									<Item regular
										style={{ marginTop: 50 }}>
										<Input placeholder='Email or Phone Number' />
										<Button transparent>
											<Text style={{ color: 'orange', fontWeight: '700' }}>Send OTP</Text>
										</Button>
									</Item>
									<Item regular style={{ marginTop: 20 }}>
										<Input placeholder='OTP' secureTextEntry={true} />
										<Button transparent>
											<Text style={{ color: 'orange', fontWeight: '700' }}>Resend</Text>
										</Button>
									</Item>
								</Form>
								<Button full
									style={{ backgroundColor: "orange", marginTop: 20, marginLeft: 20, marginRight: 20, }}
									onPress={() => { this.props.navigation.navigate('Change') }}>
									<Text style={{ color: 'white', fontWeight: 'bold' }}>RESET PASSWORD</Text>
								</Button>
							</View>
						</View>
					</View>
				</Content>
			</Container>
		)
	}

}