import React, { Component } from 'react';

import {
	View} from 'react-native';
import { Button, Text, Card, CardItem, H1, Right, Left } from 'native-base';
import CodeInput from 'react-native-confirmation-code-input';

import { requestOTP } from './../../services/AuthServices';

const MINUTES_TO_WAIT = 1;

export default class Verification extends Component {

	static navigationOptions = {
		header: null
	}
	constructor(props) {
		super(props)
		this.state = {
			api_OTP: '',
			ui_OTP: '',
			timerComplete: false,
			secondsRemaining: 0,
			seconds: 0,
			minutes: MINUTES_TO_WAIT,
			mobileNum: this.props.navigation.getParam('mobileNum'),
			otpValid: false


		}
		this.getNewOTP = this.getNewOTP.bind(this);
		this.onFinishCheckingCode = this.onFinishCheckingCode.bind(this);
	}
	componentWillMount() {
		this.getNewOTP();


	}
	componentDidMount() {
		this.startCountDown();
	}
	startCountDown() {
		let time = this.state.minutes;
		let secondsRemaining = time * 60;
		let intervalHandle = setInterval(() => {


			let min = Math.floor(secondsRemaining / 60);
			let sec = secondsRemaining - (min * 60);
			this.setState({
				minutes: min
			})
			this.setState({ seconds: sec })
			if (sec < 10) {
				let seconds = "0" + this.state.seconds
				this.setState({
					seconds: seconds,
				})
			}
			if (min < 10) {
				this.setState({
					value: "0" + min,
				})
			}
			if (min === 0 & sec === 0) {
				this.setState({ timerComplete: true });
				clearInterval(intervalHandle);
			}
			secondsRemaining--
		}, 1000);

	}

	getNewOTP() {
		let mobileNum = this.state.mobileNum;
		this.setState({ minutes: MINUTES_TO_WAIT, seconds: 0, timerComplete: false })

		console.log(mobileNum);
		if (mobileNum) {
			requestOTP(mobileNum).then((res) => {
				this.setState({ api_OTP: res });
				this.startCountDown();

			})
		}
	}
	onFinishCheckingCode(isValid) {
		this.setState({ otpValid: isValid });
	}
	render() {
		let timerComplete = this.state.timerComplete;
		return (
			<View style={{ marginTop: 100 }}>
				<Text style={{ marginTop: 50, textAlign: 'center', }}>
					<H1 style={{ color: "#003399", fontWeight: 'bold' }}>Enter OTP</H1>
				</Text>
				<Text style={{ textAlign: 'center', fontSize: 20, fontWeight: 'bold', color: '#f8bb01' }}>OTP is sent to {this.state.mobileNum}</Text>
				<CodeInput
					ref="codeInputRef2"
					secureTextEntry
					compareWithCode={this.state.api_OTP}
					activeColor='#f8bb01'
					inactiveColor='#003399'
					keyboardType="numeric"
					autoFocus={false}
					ignoreCase={true}
					inputPosition='center'
					size={50}
					containerStyle={{ marginTop: 30 }}
					codeInputStyle={{ borderWidth: 1.5 }}
					onFulfill={this.onFinishCheckingCode}
				/>
				{
					timerComplete ?
						<Card style={{ marginTop: '20%' }} transparent>
							<CardItem>
								<Left>
									<Text style={{ marginLeft: '15%' }} >Didn't recieve the OTP?</Text>
								</Left>
								<Right>
									<Button
										transparent
										onPress={this.getNewOTP}
										disabled={!timerComplete}>
										<Text style={{ color: "#f8bb01", fontWeight: 'bold', fontSize: 20, marginRight: '15%' }}>RESEND</Text>
									</Button>
								</Right>
							</CardItem>
						</Card> :
						<View style={{ marginTop: '20%' }}>
							<Text style={{ textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>
								{this.state.minutes} : {this.state.seconds}
							</Text>
						</View>
				}
				<Button full
					style={{ backgroundColor: "#f8bb01", marginTop: '10%', marginLeft: '10%', marginRight: '10%', }}
					onPress={() => { this.props.navigation.navigate('CustomerDetails') }}
					disabled={!this.state.otpValid}>
					<Text style={{ color: '#003399', fontWeight: 'bold' }}>Verify and Proceed</Text>
				</Button>
			</View>
		)
	}
}
