import React, { Component } from 'react';
import {
	StyleSheet, View, Text, Modal, ScrollView, Image
} from 'react-native';
import { Button, Drawer, Container, Header, Title, Card, Thumbnail, CardItem, Content, Left, Item, Input, InputGroup, Right, Body, Icon, H1, H3 } from 'native-base';
import { Collapse, CollapseHeader, CollapseBody, AccordionList } from 'accordion-collapse-react-native';
import EditProfile from './EditProfile';
import FooterPart from '../FooterPart';
export default class Profile extends Component {

	constructor(props) {
		super(props);
		console.log("props in profile", props);
		this.state = {
			_props: this.props,
			showEditProfile: false
		}
		this.EditProfile = this.EditProfile.bind(this);
	}
	EditProfile() {
		this.setState({ showEditProfile: !this.state.showEditProfile }, function () {
			console.log(this.state.showEditProfile);
		})
	}

	render() {
		return (
			<Container>
				<Content >
					<Image style={{ alignSelf: 'center', borderRadius: 50, marginTop: '5%' }} source={require('./../../../../assets/images/profile.jpg')} />
					<ScrollView>
						<Card style={{
							borderBottomColor: 'black',
							borderBottomWidth: 3,
						}} transparent>
							<CardItem>
								<Left>
									<Text>
										<H1 style={{ fontWeight: 'bold', }}>MANOHAR</H1>
									</Text>
								</Left>
								<Right>
									<Button
										transparent
										onPress={this.EditProfile}
									>
										<Text style={{ fontWeight: '700', fontSize: 18, color: '#f8bb01' }} >EDIT</Text>
									</Button>
								</Right>
							</CardItem>
							<Card transparent style={{
									borderBottomColor: 'black',
									borderBottomWidth: 3,
								}}>
								<CardItem>
									<Left>
										<Icon style={{ color: "#003399" }} name='md-card' />
										<Body>
											<Text style={{ fontSize:24,}}>Driver Wallet</Text>
										</Body>
									</Left>
									<Right>
										<Text style={{fontSize:24,fontWeight:'bold'}}>₹ 1360</Text>
									</Right>
								</CardItem>
							</Card>
							<CardItem style={{
								borderBottomColor: 'black',
								borderBottomWidth: 1,
								marginTop: '5%'
							}}>
								<Left >
									<Text style={{}}>PHONE NUMER</Text>
								</Left>
								<Right>
									<Text style={{}}>9700152047</Text>
								</Right>
							</CardItem>
							<CardItem style={{
								borderBottomColor: 'black',
								borderBottomWidth: 1,
								marginTop: '5%'
							}}>
								<Left >
									<Text style={{}}>EMAIL ID</Text>
								</Left>
								<Right>
									<Text style={{}}>sai32@gmail.com</Text>
								</Right>
							</CardItem>
							<CardItem style={{
								borderBottomColor: 'black',
								borderBottomWidth: 1,
								marginTop: '5%'
							}}>
								<Left >
									<Text style={{}}>LICENSE NUMBER</Text>
								</Left>
								<Right>
									<Text style={{}}>TS002361231261</Text>
								</Right>
							</CardItem>
							<CardItem style={{marginTop: '5%'}}>
								<Left >
									<Text style={{}}>RC NUMBER</Text>
								</Left>
								<Right>
									<Text style={{}}>65464647413467464</Text>
								</Right>
							</CardItem>
						</Card>

						<Card style={{
							borderBottomColor: 'black',
							borderBottomWidth: 1,
							left: 15
						}} transparent>
							<CardItem>
								<Left>
									<Text>
										<H3 style={{ fontWeight: 'bold', }}>Help</H3>
									</Text>
								</Left>
							</CardItem>
							<CardItem>
								<Left>
									<Text style={{}}>FAQs & Links</Text>
								</Left>
							</CardItem>
						</Card>


						<Card style={{
							marginLeft: '5%',
							marginRight: '5%',
						}}>
							<CardItem>
								<Left>
									<Text style={{ fontWeight: 'bold', }}>LOGOUT</Text>
								</Left>
								<Right>
									<Button
										transparent
										onPress={() => { this.props.navigation.navigate('Login') }}>
										<Icon
											style={{ color: '#f8bb01' }}
											name='md-power'
										/>
									</Button>
								</Right>
							</CardItem>
						</Card>
						<Text style={{ textAlign: 'center', fontWeight: 'bold', marginBottom: '20%' }}>APP Version 1.0.1</Text>
					</ScrollView>
				</Content>
				<FooterPart footerProps={this.props} />
				<EditProfile modalShow={this.state.showEditProfile} modalFunction={this.EditProfile} />
			</Container>
		)
	}
}

Profile.navigationOptions = ({ navigation }) => {
	return {
		header: (
			<Header>
				<Title style={{ marginTop: '5%', }}>DRIVER PROFILE</Title>
			</Header>
		)
	};
};