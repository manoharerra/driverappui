import React, { Component } from 'react';
import { StyleSheet, View, Text, Modal, Image } from 'react-native';
import { Form, Label, Button, Drawer, Container, Header, Title, Card, Thumbnail, CardItem, Content, Footer, FooterTab, Left, Item, Input, InputGroup, Right, Body, Icon, H1, H3 } from 'native-base';

export default class EditProfile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            modalShow: this.props.modalShow
        }
    }

    componentWillReceiveProps() {
        this.setState({
            modalShow: this.props.modalShow
        })
    }


    render() {
        return (
            <View>
                <Modal
                    animationType="slide"
                    visible={this.state.modalShow}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <Container>
                        <Header>
                            <Left>
                                <Button transparent dark
                                    onPress={() => {
                                        this.props.modalFunction()
                                    }}>
                                    <Text>Back</Text>
                                </Button>
                            </Left>
                            <Body>
                                <Text style={{ fontWeight: '900' }}>Edit profile</Text>
                            </Body>
                            <Right>
                                <Button transparent dark
                                    onPress={() => {
                                        this.props.modalFunction()
                                    }}>
                                    <Text>Done</Text>
                                </Button>
                            </Right>
                        </Header>
                        <Content >
                            <View style={{ marginTop: 50, marginLeft: 15 }}>
                                <Text style={{ fontWeight: 'bold' }}>
                                    EDIT ACCOUNT
                                </Text>
                            </View>
                            <Form style={{ marginTop: 20, }}>
                                <Item floatingLabel>
                                    <Label>PHONE NUMBER</Label>
                                    <Input keyboardType="numeric" />
                                </Item>
                                <Item floatingLabel last>
                                    <Label>EMAIL ADDRESS</Label>
                                    <Input />
                                </Item>
                            </Form>
                            <Button full
                                style={{ backgroundColor: "#f8bb01", marginTop: 20, marginLeft: 20, marginRight: 20, }}
                                onPress={this.ChangePassword}>
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>UPDATE</Text>
                            </Button>
                        </Content >
                    </Container>
                </Modal>
            </View>

        )
    }
}
