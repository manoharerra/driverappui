import React, { Component } from 'react';
import { Footer, FooterTab, Button, Icon, Text, View } from 'native-base';

export default class FooterPart extends Component {

	constructor(props) {
		super(props);
		this.state = {
			// selected: "key0"
		};
	}

	render() {
		return (
			<View>
				<Footer style={{ backgroundColor: '#ffedcc' }}>
					<FooterTab>
						<Button
							onPress={() => { this.props.footerProps.navigation.navigate('Main') }}>
							<Icon style={{ color: 'black' }} name="md-pin" />
							<Text style={{ color: 'black' }}>Bookings</Text>
						</Button>
						<Button
							onPress={() => { this.props.footerProps.navigation.navigate('Explore') }}>
							<Icon style={{ color: 'black' }} name="md-search" />
							<Text style={{ color: 'black' }}>Explore</Text>
						</Button>
						<Button
							onPress={() => { this.props.footerProps.navigation.navigate('Summary') }}>
							<Icon style={{ color: 'black' }} name="md-time" />
							<Text style={{ color: 'black' }}>History</Text>
						</Button>
						<Button
							onPress={() => { this.props.footerProps.navigation.navigate('Account') }}>
							<Icon style={{ color: 'black' }} name="person" />
							<Text style={{ color: 'black' }}>Profile</Text>
						</Button>
					</FooterTab>
				</Footer>
			</View >
		)
	}
}
