
import { StyleSheet } from 'react-native'
import { Constants } from 'expo'
export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
		paddingTop: Constants.statusBarHeight
	},
	logoContainer: {
		alignItems: 'center',
		flexGrow: 1,
		justifyContent: 'center',
	},
	caption: {
		color: '#003399',
		marginTop: 30,
		textAlign: 'center',
		fontSize: 25,
	},
	image: {
		width: 300,
		height: 150,
		marginLeft: 50
	},
	container2: {
		padding: 20
	},
	input: {
		height: 40,
		backgroundColor: 'white',
		marginBottom: 20,
		color: '#003399',
		paddingHorizontal: 10,
	},
	buttonContainer: {
		backgroundColor: "#003399",
		paddingVertical: 10,
		marginBottom: 150
	},
	buttonText: {
		color: '#f8bb01',
		textAlign: 'center',
		fontWeight: '900'
	},
	passwordContainer: {
		flexDirection: 'row',
		borderBottomWidth: 1,
		borderColor: '#000',
		paddingBottom: 10,
	},
	inputStyle: {
		flex: 1,
		height: 60,
		backgroundColor: 'white',
		marginBottom: 20,
		color: '#003399',
		paddingHorizontal: 10,
	},
});